<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getOrderschnage($order_status);
//$orders = $orderObj->getOrderschnage();
//echo "<pre>";print_R($orders);//get orders for super stockist and stockist.
$order_count = count($orders);
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
<thead>
	<tr>
		
		<th>
			Order Date
		</th>
		<th>
			Shop Name
		</th>
		<th>
			Orders
		</th>								
		
		 <th>
			Product Name			
		</th>	
		<th>
			Quantity
		</th>
		<th>
			Price (₹)
		</th>
		<th>
			GST Price (₹)
		</th>
	</tr>
</thead>
<tbody>
				<?php				
				foreach($orders as $key=>$value)
				{
					$orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);	
					//echo "<pre>";print_r($orderdetails);
					//$odid=$orderdetails['order_details'][0]['odid'];
					$product_name='';
					$prod_qnty='';$totalcost='';$gstcost='';
					if(count($orderdetails['order_details'])==1){						
						$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>";
						$prod_qnty='<a onclick="showOrderDetails(\''.$orderdetails['order_details'][0]['id'].'\',\'Order Details\')" title="Order Details">'.$orderdetails['order_details'][0]['product_quantity'];
						if(!empty($orderdetails['order_details'][0]['product_variant_weight1'])){
							$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight1']."-".$orderdetails['order_details'][0]['product_variant_unit1'].")";
						}
						if(!empty($orderdetails['order_details'][0]['product_variant_weight2'])){
							$prod_qnty.="(".$$orderdetails['order_details'][0]['product_variant_weight2']."-".$$orderdetails['order_details'][0]['product_variant_unit2'].")";
						}
						$prod_qnty.="</a><br>";	
						$totalcost=$orderdetails['order_details'][0]['product_total_cost']."<br>";
						$gstcost=$orderdetails['order_details'][0]['p_cost_cgst_sgst']."<br>";
					}else{
						//$tempproductname=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name'];
						foreach ($orderdetails['order_details'] as $key1=>$value1){
							$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
							$prod_qnty.='<a onclick="showOrderDetails(\''.$value1['id'].'\',\'Order Details\')" title="Order Details">'.$value1['product_quantity'];
							if(!empty($value1['product_variant_weight1'])){
								$prod_qnty.="(".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'].")";
							}
							if(!empty($value1['product_variant_weight2'])){
								$prod_qnty.="(".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'].")";
							}
							$prod_qnty.="</a><br>";
							$totalcost.=$value1['product_total_cost']."<br>";
							$gstcost.=$value1['p_cost_cgst_sgst']."<br>";
						}
					}
					
				?>
				<tr class="odd gradeX">					
						<td ><?php echo date('d-m-Y H:i:s',strtotime($value["order_date"]));?></td>
						<td ><?php echo $value["shop_name"];?></td>
						<td >
							<a onclick="showInvoice(1,<?=$value['oid'];?>)" title="View Invoice">View Invoice</a></br>
							<?php echo $value["order_no"]?></br>
							<a href="invoicedemo2.php?order_id=<?php echo $value["order_no"];?>" title="PDF Generator">View PDF</a>	
							</td>									
						<td><?php echo $product_name;?></td>
						<td align="right"><?php echo $prod_qnty;?></td>
						<td align="right"><?php echo $totalcost;?></td>
						<td align="right"><?php echo $gstcost;?></td>						
						<!--". $row["orderid"]. "-->
					</tr>
				<?php } ?>
				
				 </tbody>
</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>