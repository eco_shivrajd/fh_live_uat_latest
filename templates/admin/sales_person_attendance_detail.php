<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$report_title = $orderObj->getReportTitle();
$row = $orderObj->getSPattendance();
$record_count = mysqli_num_rows($row);
//echo "sdfsd<pre>";print_r($row);
$colspan = "9";
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? }
 ?>

<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_5">
<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if(!empty($report_title))echo $report_title; else echo "SP Attendance Report All";?></b></h4></td>              
  </tr>
  <tr>
  	<th data-filterable="false" data-sortable="true" data-direction="desc">SR NO.</th>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Date</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Start Day</th>	
	<th data-filterable="false" data-sortable="true" data-direction="desc">End Day </th>   
	<th data-filterable="false" data-sortable="false" data-direction="desc">Working Hourse</th>	
	<th data-filterable="false" data-sortable="false" data-direction="desc">Distance Travelled</th>
	<!-- <th data-filterable="false" data-sortable="true" data-direction="desc">Present Status</th>	 -->
	<th data-filterable="false" data-sortable="true" data-direction="desc">Presenty</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Remarks</th>              
  </tr>
</thead>
<tbody>					
	<?php 
	if(!empty($row))
	{
			foreach($row as $key => $value)
			{
				$leavedt = $value['leavedt'];
				/*echo "<pre>";
				print_r($row);*/
				/*$tdate1 = date('Y-m-d',strtotime($value['tdate']));
				$sp_id1 =$value['sp_id'];
				 $resultspleave = $orderObj->getSPLeavestatus($sp_id1,$tdate1);                
                if(!empty($resultspleave))
	            {					
						while($record11 = mysqli_fetch_array($resultspleave))
							{	
								$leavedt = $record11['leavedt'];
							}
				}*/		
				/*while($record11 = mysqli_fetch_array($resultspleave))
				{	
					$leavedt = $record11['leavedt'];
				}*/

				 //var_dump($resultspleave);
			/*	$frmdate = date('d-m-Y',strtotime($value['tdate']));
				$dropdownSalesPerson = $value['id'];
			    $resultsplocation = $orderObj->getSPLocationPoints($frmdate,$dropdownSalesPerson);
				$record_count_sp = mysqli_num_rows($resultsplocation);
				$i = 1;
					$countrec=0;
					$c_array_temp=array();
					if($record_count_sp > 0)
					{						
					while($record = mysqli_fetch_array($resultsplocation))
					{	
						$c_array_temp[$countrec]['id'] = $record['id'];
						$c_array_temp[$countrec]['lattitude'] = $record['lattitude'];
						$c_array_temp[$countrec]['longitude'] = $record['longitude'];
						$countrec++;
					}
					}
					$google_distance=0;
					for($i=0;$i<count($c_array_temp)-1;$i++)
					{
					$google_d= $orderObj->getDistanceBetweenPoints($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
					$google_distance=$google_distance+$google_d['distance'];
					} 
					$google_distance=bcdiv($google_distance, 1000, 3);	*/ 
			?>
					<tr class="odd gradeX">				
						<td align='right'><?=$key+1;?></td>
						<td align='Left'><?=$value['firstname'];?></td>
						<td align='right'>
                            <?php if ($leavedt=='leavedt') { ?>
                           	  <?=date('d-m-Y',strtotime($value['tdate']));?>
                           	<?php } else {
                             ?>
                              <?=date('d-m-Y',strtotime($value['leavedt']));?>  
                            <?  } ?>
							</td>
						<td align='right'><?=date('H:i:s',strtotime($value['tdate']));?></td>
						<td align='right'><?=date('H:i:s',strtotime($value['dayendtime']));?></td>
						<td align='right'><!-- <?=$value['hours_difference']?> --> <?=number_format((float)$value['hours_difference'], 0, '.', '')?> </td>
						<td align='Left'><?=number_format((float)$value['todays_travelled_distance'], 3, '.', '')?></td>
						 <td>
							 <?php if ($leavedt=='leavedt') { ?>
                           	  <b>P</b>
                           	<?php } else {
                             ?>
                              <b>A</b> 
                            <?  } ?>
                         </td> 
						<td align='right'> -							
							 </td>
					</tr>
				<?php } ?>
			<?php
	}/*else{
		echo "<tr><td colspan='5' align='center'>No matching records found</td></tr>";
	}*/
	if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>



<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_5').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>

<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
	}
} ?>
 