<!-- BEGIN HEADER -->
<?php 
//phpinfo();die();
include "../includes/grid_header.php";
include "../includes/userManage.php";	
include "../includes/shopManage.php";
include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$prodObj 	= 	new productManage($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
?>
<!-- END HEADER -->
<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "ShopOrders";	
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">Orders</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Orders</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">  
					 <div class="clearfix"></div>   
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Manage Orders
							</div>
						</div>
						<div class="portlet-body">
							<form class="form-horizontal" id="frmsearch" name="frmsearch" enctype="multipart/form-data" method="post">	
							<input type='hidden' name='user_type' id='user_type' value='<?=$_SESSION[SESSION_PREFIX.'user_type'];?>'>
							<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Admin'){ ?>
							<div class="form-group">
							  <label class="col-md-3">Order Place/Received:</label>
							  <div class="col-md-4">
							  <select name="order_received_status" id="order_received_status"  data-parsley-trigger="change" class="form-control">
								<option value="">-Select-</option>
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist'){ ?>
									<option value="2" selected>Received</option>								
									<option value="3">Placed</option>								
								<?php }else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor'){ ?>
									<option value="1" selected>Received</option>								
									<option value="2">Placed</option>			
								<?php } ?>
								</select>
							  </div>
							</div><!-- /.form-group -->	
							<?php } ?>
							<div class="form-group">
							  <label class="col-md-3">Order Delivery Manage Status:</label>
							  <div class="col-md-4">
							  <select name="order_status" id="order_status"  data-parsley-trigger="change" class="form-control">
								<option value="">-Select-</option>
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){ ?>
								<option value="1" selected>Received</option>		
								<option value="2">Assigned for Delivery</option>
								<? } ?>
								
								<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Admin'){ ?>
									<option value="2_1">Assigned for Delivery To</option>
									<option value="2_2">Assigned for Delivery From</option>
								<? } ?>
								<option value="3">Assigned to Transport office </option>								
								<option value="4">Delivered</option>
								<option value="6">Payment Received</option>
								<option value="8">Partial Payment Received</option>								
								</select>
							  </div>
							</div><!-- /.form-group -->	
												
							<div class="form-group" id="divDaily">
								<label class="col-md-3">Select Order Date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->	
							<?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){ ?>
							<div class="form-group">
								<label class="col-md-3">Superstockist:</label>
								<?php $user_result = $userObj->getAllLocalUser('Superstockist'); ?>		
								<div class="col-md-4" id="divsstockistDropdown">
									<select name="divsstockistDropdown" id="divsstockistDropdown" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<?php } ?>
							<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
							<div class="form-group">
								<label class="col-md-3">Stockist:</label>
								<?php $user_result = $userObj->getAllLocalUser('Distributor'); ?>		
								<div class="col-md-4" id="divstockistDropdown">
									<select name="divstockistDropdown" id="divstockistDropdown" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<?php } ?>
							<div class="form-group">
								<label class="col-md-3">Regional Head:</label>
								<?php $user_result = $userObj->getAllLocalUser('salesperson'); ?>		
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">State</label>
								<div class="col-md-4">
								<?php
									$sql_state="SELECT * FROM tbl_state where country_id=101";					
									$result_state = mysqli_query($con,$sql_state);		
								?>
									<select id="dropdownState" name="dropdownState" size="1" class="form-control" onChange="fnShowCity(this.value)">
									<option value="">-Select-</option>
									<?php
									while($row_state = mysqli_fetch_array($result_state))
									{?>
										<option value=<?=$row_state['id'];?>><?=fnStringToHTML($row_state['name']);?></option>
									<?php }	?>												
									</select>
								</div>
							</div>
							<div class="form-group" id="city_div">
								<label class="col-md-3">City</label>
								<div class="col-md-4" id="div_select_city">
									<select id="citySelect" size="1" class="form-control">
									<option value="">-Select-</option>									
									</select>
								</div>
							</div>
							 <div class="form-group" id="area_div">
								<label class="col-md-3">Region</label>
								<div class="col-md-4" id="div_select_area">
								 <select class="form-control"  id="subarea">
									<option value="">-Select-</option>									
								 </select>
								 </div>
							</div> 
							<div class="form-group" id="shop_div">
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4">
								<?php $shop_result = $shopObj->getAllShops(); ?>									
								 <select name="divShopdropdown" id="divShopdropdown" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_shop = mysqli_fetch_assoc($shop_result))
									{ ?>									
									<option value="<?=$row_shop['id'];?>"><?=$row_shop['name'];?></option>
									<?php } ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								<?php $cat_result = $prodObj->getAllCategory(); ?>
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php while($row_cat = mysqli_fetch_assoc($cat_result))
									{ ?>									
									<option value="<?=$row_cat['id'];?>"><?=$row_cat['categorynm'];?></option>
									<?php } ?>								
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								<?php $prod_result = $prodObj->getAllProducts(); ?>
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_prod = mysqli_fetch_assoc($prod_result))
									{ ?>									
									<option value="<?=$row_prod['id'];?>"><?=$row_prod['productname'];?></option>
									<?php } ?>	
								</select>
								</div>
								</div><!-- /.form-group -->
							
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>																		
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	
						<form class="form-horizontal" role="form" name="form" id="form" method="post" action="">	
						<div>						
							<input type="hidden" name="flag" id="flag" value="">
						<?php switch($_SESSION[SESSION_PREFIX.'user_type']){ 
								 case "Admin": ?>
								<button type="button" class="btn btn-success" id="statusbtn" name="statusbtn" data-toggle="modal">Send to Production & Assing for delivery</button>
								<span id="button_or1">OR</span> <button type="button" class="btn btn-success" id="rejectbtn" name="rejectbtn" data-toggle="modal">Reject Order</button>
								<button style="display:none" type="button" class="btn btn-success" id="statusDbtn" name="statusDbtn" data-toggle="modal">Mark As Delivered</button>
								<? break;
								case "Superstockist": ?>	
									<button type="button" class="btn btn-success" id="placeorderbtn" name="placeorderbtn">Place Order</button>
									<button type="button" style="display:none;" class="btn btn-success" id="statusbtn" name="statusbtn" data-toggle="modal">Assing for delivery</button>
								<? break;
								case "Distributor":	?>	
									<button type="button" class="btn btn-success" id="placeorderbtn" name="placeorderbtn">Place Order</button>
									<button type="button" style="display:none;" class="btn btn-success" id="statusbtn" name="statusbtn" data-toggle="modal">Assing for delivery</button>
								<? break; } ?>
						</div><br>
						<div id="order_list">
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<?php
						$order_received_status = 3;
						$order_status = '';
						if($_SESSION[SESSION_PREFIX."user_type"]=="Admin") {
							$order_status = 1;
						}
						if($_SESSION[SESSION_PREFIX."user_type"]=="Superstockist") {
							$order_received_status = 2;
						}else  if($_SESSION[SESSION_PREFIX."user_type"]=="Distributor") {
							$order_received_status = 1;
						}
						$result1 = $orderObj->getOrders_toManage($order_received_status, $order_status);				
						while($row1 = mysqli_fetch_array($result1)) { 
							$newarr['order_id']=$row1['order_id'];
							$newarr['order_date']=$row1['order_date'];
							$newarr['shop_id']=$row1['shop_id'];
							$newarr['shopnm']=$row1['shopnm'];
							$newarr['order_no']=$row1['order_no'];
							$order_noss=$row1['order_no'];
							$newarr['product_quantity']=$row1['product_quantity'];
							$newarr['p_cost_cgst_sgst']=$row1['p_cost_cgst_sgst'];
							$newarr1[]=$newarr;										
						}
						$record_count = count($newarr1);
						
						?>
						<thead>
							<tr>
							<?php if($record_count > 0){ ?>
							<th id="select_th">
								<input type="checkbox" name="select_all[]" value=""  id="select_all" onchange="javascript:checktotalchecked(this)">
							</th>
							<?php } ?>
							<th width="20%">Order Date</th>
							<th width="25%">Shop Name</th>
							<th width="25%">Orders</th>
							<th width="10%"> Quantity</th>
							<th width="10%">Unit Cost</th>
							<th width="10%">Total Cost</th>							
							</tr>
						</thead>
						<tbody>	
							<?php
									if($record_count > 0){									
									$out = array();
									foreach ($newarr1 as $key => $value){
										$countval=0;
										foreach ($value as $key2 => $value2){
											if($key2=='order_no'){
												 $index = $key2.'-'.$value2;
												if (array_key_exists($index, $out)){
													$out[$index]++;
												} else {
													$out[$index] = 1;
												}
											}
											
										}
									}
									$out1 = array_values($out);$j=0;$temp=$newarr1[0]["order_no"];$sum=0;
									
									for($i=0;$i<count($newarr1);$i++){
										$total=$newarr1[$i]['product_quantity']*$newarr1[$i]['p_cost_cgst_sgst'];	
										
										$opening_bal_details = $orderObj->get_opening_balance($newarr1[$i]['shopnm']);
										//print_r($opening_bal_details);
										$opening_balance = 0;
										$style_title = '';
										if($opening_bal_details['balance_amount'] != 0){
											$opening_balance = $opening_bal_details['balance_amount'];
											if($opening_bal_details['amount_paid'] == ''){
												$style_title = 'style="background: red;" title="Delivered & Payment Pending for previous order"';
											}else{
												$style_title = 'style="background: #D3D3DD;" title="Partial Payment done for previous order"';
											}
										}
									?>	
										<tr class="odd gradeX">										
										<?php if($temp==$newarr1[$i]["order_no"]){ 
												if($record_count > 0){ 
										?>	
										<td <?=$style_title;?>>
											 <input type="checkbox" name="select_all[]" id="select_all" value="<?=$newarr1[$i]["order_id"];?>" onchange="javascript:checktotalchecked(this)">
										</td>
												<?php } ?>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['order_date'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['shopnm'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)"><?php echo $newarr1[$i]['order_no'];?></a></td>
										<?php   $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["order_no"]; $j++; 
											} else{?>
											 <td style="display: none;"><?php echo $newarr1[$i]['order_date'];?></td> 
											 <td style="display: none;"><?php echo $newarr1[$i]['shopnm'];?></td> 
											  <td style="display: none;"><a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)"><?php echo $newarr1[$i]['order_no'];?></a></td>
											<?php } ?>
										<td align="right"><?php echo $newarr1[$i]['product_quantity'];?></td>
										<td align="right"><?php echo $newarr1[$i]['p_cost_cgst_sgst'];?></td>
										<td align="right"><?php echo $total;?></td>
										</tr>
									<?php } } ?>	
							
						</tbody>
						</table>
						</div>
						</form>
						</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<div class="modal fade" id="view_invoice" role="dialog">
	<div class="modal-dialog" style="width: 980px !important;">    
		<!-- Modal content-->
		<div class="modal-content" id="view_invoice_content">      
		</div>      
	</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="assign_delivery" role="dialog" style="height:auto;">
    <div class="modal-dialog">    
  <!-- Modal content-->
  <div class="modal-content" style="height:270px;" >
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
	  <h4 class="modal-title"><?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){ ?>
	  Send to Production & <?php } ?>Assign for Delivery</h4>
	</div>
	<div class="modal-body" style="padding-bottom: 5px !important;">
	<form class="form-horizontal">
		<input type="hidden" value="" id="orderids" name="orderids">
		<div class="form-group">
			<label class="col-md-6">Assignment Date:<span class="mandatory">*</span></label>
		  <div class="col-md-6">
		  <div class="input-group date date-picker2" data-date-format="dd-mm-yyyy">
			<input type="text" name="del_assigned_date" id="del_assigned_date" class="form-control" placeholder="Assignment Date">
			<span class="input-group-btn">
			<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
			</div>
		  </div>
		</div>
		<div class="clearfix"></div>
		<div class="form-group">
				<label class="col-md-6" style="padding-top:5px">Assign To:<span class="mandatory">*</span></label>
				<div class="col-md-6" id="divdpDropdown">
					<select name="selectdp" id="selectdp" class="form-control">	
						<?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){ ?>
						<option value="Superstockist">Superstockist</option>
						<?php } if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist'){ ?>
						<option value="Distributor">Stockist</option>
						<?php } ?>
						<option value="DeliveryPerson">Delivery Person</option>
					</select>
				</div>
			</div><!-- /.form-group -->
			<div class="clearfix"></div>
		
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<button type="button" name="btn_assigndelivery" id="btn_assigndelivery" class="btn btn-primary" data-dismiss="modal">Assign</button>
				 				
			</div>
		</div>
	</form>
	</div>	
  </div>
  </div>
</div>
<div class="modal fade" id="assign_delivered" role="dialog" style="height:auto;">
    <div class="modal-dialog">    
  <!-- Modal content-->
  <div class="modal-content" style="height:270px;" >
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
	  <h4 class="modal-title">Mark As Delivered</h4>
	</div>
	<div class="modal-body" style="padding-bottom: 5px !important;">
	<form class="form-horizontal">
		<input type="hidden" value="" id="orderids4" name="orderids4">
		<div class="form-group">
			<label class="col-md-6">Delivery Date:<span class="mandatory">*</span></label>
		  <div class="col-md-6">
		  <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
			<input type="text" name="del_assigned_date1" id="del_assigned_date1" class="form-control" placeholder="Delivery Date">
			<span class="input-group-btn">
			<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
			</div>
		  </div>
		</div>
		<div class="clearfix"></div>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<button type="button" name="btn_delivered" id="btn_delivered" class="btn btn-primary" data-dismiss="modal">Update</button>
				 				
			</div>
		</div>
	</form>
	</div>	
  </div>
  </div>
</div>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
$(document).ready(function() {
	$("#select_th").removeAttr("class");
	$("#main_th th").removeAttr("class");
	if ( $.fn.dataTable.isDataTable( '#sample_2' ) ) {
		$("#sample_2").dataTable().fnDestroy()

		$('#sample_2').dataTable( {
			order: [],
			columnDefs: [ { orderable: false, targets: [0,1,2,3,4,5] } ]
		});
	}
});
$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
$('.date-picker2').datepicker({
	rtl: Metronic.isRTL(),
	startDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function fnShowStockist(id) { 	
	var url = "getStockistDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divStocklistDropdown");	
	document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";
}
function fnShowSalesperson(id) {
	var url = "getSalesPersonDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divsalespersonDropdown");	 
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
	FnGetShopsDropdown("");
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetShopsDropdown";
	CallAJAX(url,"div_select_area");
	FnGetShopsDropdown("");
}


function showInvoice(id) {	
	var url = "invoice.php"; 
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_id='+id,
		async: false
	}).done(function (response) {
		console.log(response);
		showInvoice1(id,response);
		$('#view_invoice_content').html(response);
		$('#view_invoice').modal('show');
	}).fail(function () { });
	return false;
}
function showInvoice1(id,response) {
		//console.log(response);
	var url = "invoicedemo2.php"; 
    jQuery.ajax({
		url: url,
		method: 'GET',
		data: 'oids='+id,
		async: false
	}).done(function (response) {
		//console.log(response);
	}).fail(function () { });
	return false;
}
function takeprint_invoice() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\
	.darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
	.fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
	.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
	.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
	.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
	.table-bordered-popup {    border: 1px solid #364622;}\
	.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
		border: 1px solid #364622;	color:#4a5036;}\
	.blue{	color:#010057;}\
	.blue1{	color:#574960;	font-size:16px;}\
	.buyer_section{	color:#574960;	font-size:14px;}\
	.pad-5{	padding-left:10px;}\
	.pad-40{	padding-left:40px;}\
	.np{	padding-left:0px;	padding-right:0px;}\
	.bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
	 background-size: 200px 200px;}\
	</style>' + $("#divPrintArea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}

function checktotalchecked(obj){
	$("#select_th").removeAttr("class");
	if($(obj).is(':checked') == true)
	{		
		var checkbox_count = ($('input:checkbox').length) - 1;
		var check_count = ($('input:checkbox:checked').length);
		if(obj.value == ''){
			$('input:checkbox').attr('checked', true);
		}
		else if(checkbox_count == check_count)
			$('input:checkbox').attr('checked', true);
				
	}else{		
		$('#select_all').attr('checked', false);
		if(obj.value == '')
			$('input:checkbox').attr('checked', false);
	}
}
function ShowReport() {	
	var order_received_status = $('#order_received_status').val();
	var user_type = $('#user_type').val();
	if(user_type == 'Superstockist'){
		if(order_received_status == 2)
			$("#placeorderbtn").show();
		else
			$("#placeorderbtn").hide();
	}
	else if(user_type == 'Distributor'){
		if(order_received_status == 1)
			$("#placeorderbtn").show();
		else
			$("#placeorderbtn").hide();
	}
	var order_status = $('#order_status').val();
	if(order_status != '' && $('#order_received_status').val() != undefined) {//if(order_status == '2_1' || order_status == '2_2'){
		$('#order_received_status').get(0).selectedIndex = 0;
		$("#placeorderbtn").hide();
	}
	if(order_status != ''){
		$("#placeorderbtn").hide();
	}
	/*if(order_received_status == '1' || order_received_status == '2'){
		$('#order_status').get(0).selectedIndex = 0;
	}*/
	
	if(order_status!='1'){
		$("#statusbtn").hide();
		$("#rejectbtn").hide();
		$("#button_or1").hide();
	}else{
		$("#statusbtn").show();
		$("#rejectbtn").show();
		$("#button_or1").show();
	}
	if(order_status!='3'){
		$("#statusDbtn").hide();
	}else{
		$("#statusDbtn").show();
	}
	if(order_status == '2_1'){
		$("#statusbtn").show();
	}else{
		$("#statusbtn").hide();
	}
	var url = "ajax_show_orders.php"; 
	
	var data = $('#frmsearch').serialize();
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {		 
		$('#order_list').html(response);
		var table = $('#sample_2').dataTable();      
		table.fnFilter('');
		$("#select_th").removeAttr("class");
	}).fail(function () { });
	return false;
}

$( "#placeorderbtn" ).click(function() {
	var check_count = ($('input:checkbox:checked').length);
	$('#flag').val('placeorder');
	if(check_count == 0){
		alert('Please select order to place');
		return false;
	}else{
		var url = "updateOrder.php"; 
		var data = $('#form').serialize();
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			if(response == 'success'){
				alert('Order placed successfully');
				location.reload();
			}
			else
				alert('Unable to place order, please try again');
		}).fail(function () { });
		return false;
	}
});
$( "#rejectbtn" ).click(function() {
	var check_count = ($('input:checkbox:checked').length);
	$('#flag').val('reject');
	if(check_count == 0){
		alert('Please select order to reject');
		return false;
	}else{
		var url = "updateOrderDelivery.php"; 
		var data = $('#form').serialize();
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			alert(response);
		}).fail(function () { });
		return false;
	}
});
$('.date-picker2').datepicker({
	rtl: Metronic.isRTL(),
	startDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
$("#statusbtn").click(function(){
   var void1 = [];			
	$('input[name="select_all[]"]:checked').each(function() {			 
	   void1.push(this.value);			   
	});
	var ids = void1.join();
	if(ids!=''){			
		$("#assign_delivery").modal('show');
		$("#orderids").val(ids);		
	}else{
		alert("Please select minimum one order");
		//location.reload();
	}
	
});
$( "#btn_assigndelivery" ).click(function() {
	var selectdp = $('#selectdp').val();
	var orderids = $("#orderids").val();
	var del_assigned_date = $("#del_assigned_date").val();
	if(del_assigned_date!=''){
		var url = 'updateOrder.php';
		var data = 'flag=assign_delivery&selectdp='+selectdp+'&del_assigned_date='+del_assigned_date+'&orderids='+orderids;
		//alert(url);
		 jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			if(response == 'success'){
				alert('Order assigned for delivery');
				location.reload();
			}
			else
				alert('Unable to place order, please try again');
		}).fail(function () { });
	}else{
		alert("Please select date");
		return false;
	}
});
$("#statusDbtn").click(function(){
	
   var void1 = [];			
	$('input[name="select_all[]"]:checked').each(function() {			 
	   void1.push(this.value);			   
	});
	var orderids = void1.join();
	if(orderids!=''){			
		$("#assign_delivered").modal('show');
		$("#orderids4").val(orderids);		
	}else{
		alert("Please select minimum one order");
		//location.reload();
	}
	
});
$( "#btn_delivered" ).click(function() {
	var orderids4 = $("#orderids4").val();
	var delivered_date = $("#del_assigned_date1").val();
	if(delivered_date!=''){
		var url = 'updateOrder.php';
		var data = 'flag=delivered&delivered_date='+delivered_date+'&orderids='+orderids4;
		//alert(url);
		 jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			if(response == 'success'){
				alert("Orders marked as delivered");
				location.reload();
			}
			else
				alert("Unable to update order, please try again");
		}).fail(function () { });
	}else{
		alert("Please select date");
		return false;
	}
});
$( "#rejectbtn" ).click(function() {
	var check_count = ($('input:checkbox:checked').length);
	$('#flag').val('reject');
	if(check_count == 0){
		alert('Please select order to reject');
		return false;
	}else{
		var url = "updateOrderDelivery.php"; 
		var data = $('#form').serialize();
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			alert(response);
		}).fail(function () { });
		return false;
	}
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>