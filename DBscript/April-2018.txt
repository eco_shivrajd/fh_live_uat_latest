
--added by shivraj
--11-04-2018
ALTER TABLE `tbl_product` ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `productname`;
ALTER TABLE `tbl_product` ADD `deleted_on` DATETIME NOT NULL AFTER `isdeleted`;

ALTER TABLE `tbl_category` ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `categoryimage`, ADD `deleted_on` DATETIME NOT NULL AFTER `isdeleted`;
ALTER TABLE `tbl_brand` ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `type`, ADD `deleted_on` DATETIME NOT NULL AFTER `isdeleted`;
ALTER TABLE `tbl_order_details` ADD `supp_chnz_status` INT NOT NULL DEFAULT '1' AFTER `order_status`;

ALTER TABLE `tbl_sp_tadabill` ADD `Current_rate_mot` DECIMAL(10,2) NOT NULL AFTER `date_tada`;